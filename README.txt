
#################################
## e-Commerce Webform Products ##
#################################

-- Quick Questions --

Q: What does this module do?
A: It basically improves how e-Commerce interacts with the webform module by doing the
 following:
 - Allows adding products to the cart on submission of the form.
 - Changes the 'submit' button on the client form to 'Add to cart' when above is enabled.
 
Q: How do I install the module?
A: 1 - Download and untar e-Commerce 4 (http://drupal.org/project/ecommerce), all of it's
 dependent modules, and Webform (http://drupal.org/project/webform) into a modules folder,
 eg sites/all/modules.
 2 - Go to the modules page at example.com/admin/build/modules and enable e-Commerce, it's dependent modules and Webform.

-- Module Configuration --

This module allows you to configure each webform individually. 

In the 'configuration' tab under the 'Webform Settings' fieldset, head to the 'confirmation message or redirect URL' textarea. In here, you can still do everything that webform provides (EG: message: internal: etc...). 

However, with this module enabled, you can now enter a string in the  following format:

addtocart:message to display:path/to/redirect
eg: addtocart:Successfully added [current-node-title] to your shopping cart!:products

- 'addtocart' tells this module you want to add this to the cart when submitting the form.
- 'Successfully added [current-node-title] to your shopping cart!' - Is a message that is displayed on the next page. The token replacement string ([current-node-title]) is provided by this module, see the EXTRAS section.
- 'products' - This is the URL do redirect to.

-- Extras --

Themeable everything
 - If you want to override any of messages, you can do that in your theme settings.php file
 
Token replacement string
 - This module provides some replacement string for the token module. See the token help table for more info.